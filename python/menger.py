
class MengerSponge(object):

  def __init__(self,n,size):
    self.n = n
    self.s = size

  def inside(self,i,j,k):
    if i < 0 or j < 0 or k <0:
      return False
    if i >= 3**self.n or j >= 3**self.n or k >= 3**self.n:
      return False
    for p in range(self.n):
      if (i // 3**p) % 3 == 1 and (j // 3**p) % 3 == 1:
        return False
      if (i // 3**p) % 3 == 1 and (k // 3**p) % 3 == 1:
        return False
      if (j // 3**p) % 3 == 1 and (k // 3**p) % 3 == 1:
        return False
    return True    

  def v(self,i,j,k):
    return(1+i+(3**self.n+1)*j+(3**self.n+1)**2*k)

  def save(self,filename):
    with open(filename,mode='w') as f:
      for k in range(3**self.n+1):
        for j in range(3**self.n+1):
          for i in range(3**self.n+1):
            f.write("v {:4f} {:4f} {:4f}\n".format(i*self.s/(3**self.n),j*self.s/(3**self.n),k*self.s/(3**self.n)))

      for k in range(3**self.n): 
        for j in range(3**self.n):
            for i in range(3**self.n):
                if self.inside(i,j,k):
                  if not self.inside(i,j,k-1): ##face dessous
                    f.write("f {} {} {} {}\n".format(self.v(i,j,k),self.v(i,j+1,k),self.v(i+1,j+1,k),self.v(i+1,j,k)))
                  if not self.inside(i-1,j,k): ##face gauche
                    f.write("f {} {} {} {}\n".format(self.v(i,j,k),self.v(i,j,k+1),self.v(i,j+1,k+1),self.v(i,j+1,k)))
                  if not self.inside(i,j-1,k): ##face devant
                    f.write("f {} {} {} {}\n".format(self.v(i,j,k),self.v(i+1,j,k),self.v(i+1,j,k+1),self.v(i,j,k+1)))
                  if not self.inside(i+1,j,k): ##face droite
                    f.write("f {} {} {} {}\n".format(self.v(i+1,j,k),self.v(i+1,j+1,k),self.v(i+1,j+1,k+1),self.v(i+1,j,k+1)))
                  if not self.inside(i,j+1,k): ##face arriere
                    f.write("f {} {} {} {}\n".format(self.v(i,j+1,k),self.v(i,j+1,k+1),self.v(i+1,j+1,k+1),self.v(i+1,j+1,k)))
                  if not self.inside(i,j,k+1): ##face dessus
                    f.write("f {} {} {} {}\n".format(self.v(i,j,k+1),self.v(i+1,j,k+1),self.v(i+1,j+1,k+1),self.v(i,j+1,k+1)))

m = MengerSponge(4,1.0)

m.save('menger4.obj')

